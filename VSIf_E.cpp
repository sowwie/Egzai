#include <iostream>
#include <iomanip>
int main(){
	int n;
	std::cin >> n;
	double matrica [10][10];
	for (int q = 0; q < n; q++){
		for (int w = 0; w < n; w++){
			std::cin >> matrica [q][w];
		}
	}
	double suma = 0;
	double kiekis = 0;
	int katra = 0;
	//nelyginis
	if (n % 2 != 0){
		for (int s = 1; s <= n / 2; s++){
		katra++;
			for (int e = 0; e < katra; e++){
				suma = suma + matrica [s][e];
				kiekis++;
			}
		}
	katra = 0;
		for (int z = n-2; z >= (n / 2) + 1; z--){
			katra++;
			for (int g = 0; g < katra; g++){
				suma = suma + matrica [z] [g];
				kiekis++;
			}
		}
	suma = suma / kiekis;
		
	}
	
	//lyginis
	if ( n % 2 == 0){
		for (int o = 1; o < n / 2; o++){
			katra++;
			for (int l = 0; l < katra; l++){
			suma = suma + matrica [o] [l];
			kiekis++;
			}
		}
		katra = 0;
		for (int h = n - 2; h > (n / 2) - 1; h--){
			katra++;
			for (int k = 0; k < katra; k++){
			suma = suma + matrica [h][k];
			kiekis++;
			}
		}
		suma = suma / kiekis;	
	}	
	std::cout << std::fixed << std::setprecision(2) << suma;
}
